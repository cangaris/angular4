import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from "../../shared/services/user.service";
import { Router } from "@angular/router";
import { FormBuilder, Validators } from "@angular/forms";
import { Subscription } from "rxjs";

interface LoginData {username: string; password: string}

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit, OnDestroy {

  private sub = new Subscription();

  form = this.fb.group({
    username: ['basia@onet.pl', [
      Validators.email,
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(20),
    ]],
    password: ['secretpass2', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(20),
    ]],
  })

  constructor(private userService: UserService,
              private router: Router, private fb: FormBuilder) {}

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  login(): void {
    if (this.form.invalid) {
      return alert('form is invalid');
    }
    this.userService.login(this.form.value as LoginData)
      .subscribe({
        next: () => {
          this.userService.changeLogged(true);
          void this.router.navigate(['/users']);
        },
        error: () => this.userService.changeLogged(false),
      })
  }

  ngOnInit(): void {
    const s3 = this.userService.isLogged()
      .subscribe(value => console.log('login info', value))
    this.sub.add(s3);

    const s1 = this.form.get('username')?.valueChanges
      .subscribe( (value) => console.log('username val', value))
    this.sub.add(s1);

    const s2 = this.form.get('username')?.statusChanges
      .subscribe( (value) => console.log('username stat', value))
    this.sub.add(s2);
  }
}
