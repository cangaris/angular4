import { Component, OnInit } from '@angular/core';
import { User } from "../../shared/models/User";
import { UserService } from "../../shared/services/user.service";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  users: User[] = [];
  loading = false;
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUserData();
  }

  onInputChange(event: Event): void {
    const inputValue = (event.target as HTMLInputElement).value;
    this.getUserData(inputValue);
  }

  getUserData(searchValue?: string): void {
    this.loading = true;
    this.userService.getUser(searchValue)
      .subscribe({
        next: value => this.users = value,
        complete: () => this.loading = false
      })
  }
}
