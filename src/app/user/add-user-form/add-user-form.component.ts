import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserService } from "../../shared/services/user.service";
import { User } from "../../shared/models/User";

@Component({
  selector: 'app-add-user-form',
  templateUrl: './add-user-form.component.html',
  styleUrls: ['./add-user-form.component.css']
})
export class AddUserFormComponent implements OnInit {

  @Input() user?: User;
  @Output() userEmitter = new EventEmitter<User>();
  form: User = {
    id: undefined,
    firstName: '',
    lastName: '',
    email: '',
    role: '',
    password: '',
  }

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    if (this.user) {
      this.form.id = this.user.id;
      this.form.firstName = this.user.firstName;
      this.form.lastName = this.user.lastName;
      this.form.role = this.user.role;
      this.form.email = this.user.email;
      this.form.password = this.user.password;
    }
  }

  onSubmit(): void {
    const fn = this.user?.id
      ? this.userService.editUser(this.form)
      : this.userService.addUser(this.form);
    fn.subscribe({
      next: value => {
        this.userEmitter.emit(value)
        this.resetForm();
      }
    })
  }

  resetForm(): void {
    this.form = {
      id: undefined,
      firstName: '',
      lastName: '',
      role: '',
      email: '',
    };
  }
}
