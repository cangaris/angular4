import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from "../shared/models/User";
import { UserService } from "../shared/services/user.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {

  @Output() userEmitter = new EventEmitter<boolean>();
  @Input() user: User | undefined;
  insideElement: boolean = false;
  showDetails: boolean = false;
  date = new Date();

  constructor(private userService: UserService) {
  }

  moveEvent(mode: boolean) {
    this.insideElement = mode;
  }

  showDetailsFn(mode: boolean) {
    this.showDetails = mode;
  }

  removeUser(user: User) {
    this.userService.delUser(user.id)
      .subscribe({
        next: () => this.userEmitter.emit()
      })
  }
}
