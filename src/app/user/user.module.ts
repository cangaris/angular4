import { NgModule } from '@angular/core';
import { UserComponent } from "./user.component";
import { SharedModule } from "../shared/shared.module";
import { AddUserFormComponent } from './add-user-form/add-user-form.component';
import { UsersListComponent } from './users-list/users-list.component';
import { LoginFormComponent } from './login-form/login-form.component';

@NgModule({
  declarations: [
    UserComponent,
    AddUserFormComponent,
    UsersListComponent,
    LoginFormComponent,
  ],
  exports: [
    UserComponent,
    AddUserFormComponent
  ],
  imports: [
    SharedModule,
  ]
})
export class UserModule { }
