import { Component, OnInit } from '@angular/core';
import { User } from "./shared/models/User";
import { UserService } from "./shared/services/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
