import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HelloComponent } from './hello/hello.component';
import { TransformCasePipe } from './shared/pipes/transform-case.pipe';
import { UserModule } from "./user/user.module";
import { SharedModule } from "./shared/shared.module";
import { RouterModule, Routes } from "@angular/router";
import { LoginFormComponent } from "./user/login-form/login-form.component";
import { UsersListComponent } from "./user/users-list/users-list.component";
import { IsLoggedInGuard } from "./shared/guards/is-logged-in.guard";
import { IsLoggedOutGuard } from "./shared/guards/is-logged-out.guard";

const routes = [{
  path: "login",
  component: LoginFormComponent,
  canActivate: [IsLoggedOutGuard] // dla niezalogowanego
}, {
  path: "users",
  component: UsersListComponent,
  canActivate: [IsLoggedInGuard] // dla zalogowanego
}, {
  path: "hello",
  component: HelloComponent // publiczny
}] as Routes;

@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    SharedModule,
    UserModule
  ],
  providers: [],
  exports: [
    TransformCasePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
