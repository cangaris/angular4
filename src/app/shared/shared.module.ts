import { NgModule } from '@angular/core';
import { TransformCasePipe } from "./pipes/transform-case.pipe";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NavbarComponent } from './navbar/navbar.component';
import { RouterLink } from "@angular/router";
@NgModule({
  declarations: [
    TransformCasePipe,
    NavbarComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    RouterLink,
  ],
  exports: [
    TransformCasePipe,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    NavbarComponent,
  ]
})
export class SharedModule {}
