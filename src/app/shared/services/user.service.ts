import { EventEmitter, Injectable } from '@angular/core';
import { User } from "../models/User";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiUserUrl = environment.urlApi + "/api/users";
  private config = { withCredentials: true }; // global config
  private isLoggedValue = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {}

  isLogged(): Observable<boolean> {
    return this.isLoggedValue.asObservable();
  }

  changeLogged(isLogin: boolean): void {
    this.isLoggedValue.next(isLogin);
  }

  login(loginData: {username: string; password: string}): Observable<void> {
    return this.http.post<void>(`${this.apiUserUrl}/login`, loginData,
      { withCredentials: true }
    );
  }

  logout(): Observable<void> {
    return this.http.get<void>(`${this.apiUserUrl}/logout`, {...this.config});
  }

  getUser(searchValue?: string): Observable<User[]> {
    const params: {q?: string} = {};
    if (searchValue) {
      params.q = searchValue
    }
    return this.http.get<User[]>(this.apiUserUrl, { ...this.config, params });
  }

  addUser(user: User): Observable<User> {
    return this.http.post<User>(this.apiUserUrl, user, {...this.config})
  }

  editUser(user: User): Observable<User> {
    return this.http.put<User>(`${this.apiUserUrl}/${user.id}`, user, {...this.config})
  }

  delUser(userId: User["id"]): Observable<void> {
    return this.http.delete<void>(`${this.apiUserUrl}/${userId}`, {...this.config})
  }
}
