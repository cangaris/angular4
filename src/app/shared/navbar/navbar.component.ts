import { Component, OnInit } from '@angular/core';
import { UserService } from "../services/user.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  constructor(private userService: UserService, private router: Router) { }

  logout(): void {
    this.userService.logout()
      .subscribe({
        next: () => {
          this.userService.changeLogged(false);
          void this.router.navigate(['/login']);
        },
      })
  }
}
