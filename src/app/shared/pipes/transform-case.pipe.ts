import { Pipe, PipeTransform } from '@angular/core';

type PipeOptions = 'lowercase' | 'uppercase';

@Pipe({
  name: 'transformCase'
})
export class TransformCasePipe implements PipeTransform {
  transform(value: string, arg?: PipeOptions): string {
    if (arg === 'lowercase') {
      return value.toLowerCase();
    }
    return value.toUpperCase();
  }
}
